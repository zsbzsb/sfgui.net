﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Desktop : InternalBase
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        private EventHookups _eventhookups = null;
        #endregion

        #region Properties
        public Engine Engine
        {
            get
            {
                EnsureValid();
                return new Engine(GetEngine(_pointer));
            }
        }
        #endregion

        #region Constructors
        public Desktop()
        {
            _pointer = CreateDesktop();
            _eventhookups = new EventHookups(HandleEvent);
        }
        #endregion

        #region Functions
        public void AttachEvents(SFML.Window.Window Window)
        {
            _eventhookups.AttachEvents(Window);
        }
        public void DetachEvents(SFML.Window.Window Window)
        {
            _eventhookups.DetachEvents(Window);
        }
        protected override void Destroy()
        {
            DestroyDesktop(_pointer);
            base.Destroy();
        }
        public void Add(Widget Widget)
        {
            EnsureValid();
            Add(_pointer, Widget.WidgetPointer);
        }
        public void BringToFront(Widget Widget)
        {
            EnsureValid();
            BringToFront(_pointer, Widget.WidgetPointer);
        }
        public void HandleEvent(Event Event)
        {
            EnsureValid();
            HandleEvent(_pointer, ref Event);
        }
        public bool LoadThemeFromFile(string Filename)
        {
            EnsureValid();
            return LoadThemeFromFile(_pointer, Filename);
        }
        public void Refresh()
        {
            EnsureValid();
            Refresh(_pointer);
        }
        public void Remove(Widget Widget)
        {
            EnsureValid();
            Remove(_pointer, Widget.WidgetPointer);
        }
        public void RemoveAll()
        {
            EnsureValid();
            RemoveAll(_pointer);
        }
        public void Update(float Seconds)
        {
            EnsureValid();
            Update(_pointer, Seconds);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_Create"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr CreateDesktop();

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyDesktop(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_Add"), SuppressUnmanagedCodeSecurity]
        private static extern void Add(IntPtr Pointer, IntPtr Widget);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_BringToFront"), SuppressUnmanagedCodeSecurity]
        private static extern void BringToFront(IntPtr Pointer, IntPtr Widget);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_GetEngine"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetEngine(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_HandleEvent"), SuppressUnmanagedCodeSecurity]
        private static extern void HandleEvent(IntPtr Pointer, ref Event Event);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_LoadThemeFromFile"), SuppressUnmanagedCodeSecurity]
        private static extern bool LoadThemeFromFile(IntPtr Pointer, string Filename);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_Refresh"), SuppressUnmanagedCodeSecurity]
        private static extern void Refresh(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_Remove"), SuppressUnmanagedCodeSecurity]
        private static extern void Remove(IntPtr Pointer, IntPtr Widget);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_RemoveAll"), SuppressUnmanagedCodeSecurity]
        private static extern void RemoveAll(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgDesktop_Update"), SuppressUnmanagedCodeSecurity]
        private static extern void Update(IntPtr Pointer, float Seconds);
        #endregion
    }
}
