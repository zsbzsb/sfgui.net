﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Renderer : InternalBase
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Constructors
        internal Renderer(IntPtr Pointer)
        {
            _pointer = Pointer;
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyRenderer(_pointer);
            base.Destroy();
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is Renderer) return IsEqual(_pointer, (Object as Renderer)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRenderer_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyRenderer(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRenderer_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);
        #endregion
    }
}
