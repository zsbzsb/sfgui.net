﻿using System;

namespace SFGUINet
{
    internal interface NativeCollectionSource<T>
    {
        int GetItemCount();
        T GetItemByIndex(int Index);
    }
}
