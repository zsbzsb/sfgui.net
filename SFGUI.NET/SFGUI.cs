﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class SFGUI : InternalBase
    {
        #region Variables
        public IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        public Renderer Renderer
        {
            get
            {
                EnsureValid();
                return new Renderer(GetRenderer(_pointer));
            }
        }
        #endregion

        #region Constructors
        public SFGUI()
        {
            _pointer = CreateSFGUI();
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroySFGUI(_pointer);
            base.Destroy();
        }
        public void Display(SFML.Window.Window Target)
        {
            EnsureValid();
            DisplayWindow(_pointer, Target.CPointer);
        }
        public void Display(RenderWindow Target)
        {
            EnsureValid();
            DisplayRenderWindow(_pointer, Target.CPointer);
        }
        public void Display(RenderTexture Target)
        {
            EnsureValid();
            DisplayRenderTexture(_pointer, Target.CPointer);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgSFGUI_Create"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr CreateSFGUI();

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgSFGUI_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroySFGUI(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgSFGUI_DisplayWindow"), SuppressUnmanagedCodeSecurity]
        private static extern void DisplayWindow(IntPtr Pointer, IntPtr Window);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgSFGUI_DisplayRenderWindow"), SuppressUnmanagedCodeSecurity]
        public static extern void DisplayRenderWindow(IntPtr Pointer, IntPtr RenderWindow);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgSFGUI_DisplayRenderTexture"), SuppressUnmanagedCodeSecurity]
        private static extern void DisplayRenderTexture(IntPtr Pointer, IntPtr RenderTexture);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgSFGUI_GetRenderer"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetRenderer(IntPtr Pointer);
        #endregion
    }
}
