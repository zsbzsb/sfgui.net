﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using SFML.Window;
using SFML.Graphics;
using SFGUINet;

namespace SFGT
{
    static class Test
    {
        public static void Main()
        {
            RenderWindow renderwindow = new RenderWindow(new VideoMode(500, 500), "SFGUI.NET Test");
            renderwindow.SetVerticalSyncEnabled(true);

            SFGUI sfgui = new SFGUI();
            Label label = new Label("Hello World! SFML.NET");

            var btn = new RadioButton("I am a radio button!");

            Box box = new Box(Box.Orientation.Vertical, 5f);
            box.Pack(label);
            box.Pack(btn);
            box.Pack(new RadioButton("second", btn.Group));
            box.Pack(new RadioButton("third", btn.Group));

            SFGUINet.Window window = new SFGUINet.Window();
            window.Title = "Hello World!";
            window.Add(box);

            Desktop desktop = new Desktop();
            desktop.Add(window);

            renderwindow.ResetGLStates();

            desktop.AttachEvents(renderwindow);

            while (renderwindow.IsOpen ())
            {
                renderwindow.DispatchEvents();
                
                desktop.Update(1f / 30f);
                renderwindow.Clear(Color.Blue);
                sfgui.Display(renderwindow);
                renderwindow.Display();
            }
        }
    }
}
