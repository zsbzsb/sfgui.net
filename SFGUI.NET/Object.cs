﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Object : InternalBase
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr ObjectPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        #endregion

        #region Constructors
        internal Object() { }
        protected virtual void Initialize(IntPtr Pointer)
        {
            _pointer = Pointer;
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyObject(_pointer);
            base.Destroy();
        }
        public Signal GetSignal(SignalID ID)
        {
            EnsureValid();
            if (ID.Pointer == IntPtr.Zero)
            {
                ID.Pointer = GetSignalID(_pointer, ID.Type);
            }
            return new Signal(GetSignal(_pointer, ID.Pointer));
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgObject_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyObject(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgObject_GetSignal"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetSignal(IntPtr Pointer, IntPtr SignalID);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgSignalID_GetID"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetSignalID(IntPtr Pointer, SignalIDType Type);
        #endregion
    }
}
