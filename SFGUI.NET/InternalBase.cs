﻿using System;
using System.Diagnostics;

namespace SFGUINet
{
    /// <summary>DO NOT USE - for internal use only</summary>
    [DebuggerTypeProxy(typeof(FlattenedDebugView))]
    public abstract class InternalBase : IDisposable
    {
        #region Variables
        private bool _disposed = false;
        #endregion

        #region Properties
        protected bool Disposed
        {
            get
            {
                return _disposed;
            }
        }
        #endregion

        #region Constructors/Destructors
        ~InternalBase()
        {
            Dispose();
        }
        #endregion

        #region Functions
        public void Dispose()
        {
            if (!_disposed)
            {
                try
                {
                    Destroy();
                }
                catch (Exception e)
                {
                #if DEBUG // only rethrow the exception if we are in debug mode
                    throw e;
                #else
                    Console.WriteLine("Suppresed exception during Dispose() call - run in debug mode to throw the exception.");
                #endif
                }
                _disposed = true;
                GC.SuppressFinalize(this);
            }
        }
        protected virtual void Destroy() { }
        protected void EnsureValid()
        {
            if (_disposed) throw new Exception("Attempt to access disposed object");
        }
        public override bool Equals(object obj)
        {
            return (obj is InternalBase) && (obj as InternalBase) == this;
        }
        protected virtual bool IsEqual(InternalBase Object)
        {
            return object.ReferenceEquals(this, Object);
        }
        #endregion

        #region Operators
        public static bool operator ==(InternalBase A, InternalBase B)
        {
            if (object.ReferenceEquals(A, null)) return object.ReferenceEquals(B, null);
            if (object.ReferenceEquals(B, null)) return object.ReferenceEquals(A, null);
            A.EnsureValid();
            B.EnsureValid();
            return A.IsEqual(B);
        }
        public static bool operator !=(InternalBase A, InternalBase B)
        {
            return !(A == B);
        }
        #endregion
    }
}
