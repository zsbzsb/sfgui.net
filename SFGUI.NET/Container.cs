﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Container : Widget
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr ContainerPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        public NativeCollection<Widget> Children
        {
            get
            {
                EnsureValid();
                return new NativeCollection<Widget>(new ContainerCollectionSource(this));
            }
        }
        #endregion

        #region Constructors
        internal Container() { }
        internal Container(IntPtr Pointer) : base(GetBaseWidget(Pointer))
        {
            _pointer = Pointer;
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyContainer(_pointer);
            base.Destroy();
        }
        protected override void Initialize(IntPtr Pointer)
        {
            _pointer = Pointer;
            base.Initialize(GetBaseWidget(Pointer));
        }
        public void Add(Widget Widget)
        {
            EnsureValid();
            Add(_pointer, Widget.WidgetPointer);
        }
        public void HandleChildInvalidate(Widget Widget)
        {
            EnsureValid();
            HandleChildInvalidate(_pointer, Widget.WidgetPointer);
        }
        public bool IsChild(Widget Widget)
        {
            EnsureValid();
            return IsChild(_pointer, Widget.WidgetPointer);
        }
        public void Remove(Widget Widget)
        {
            EnsureValid();
            Remove(_pointer, Widget.WidgetPointer);
        }
        public void RemoveAll()
        {
            EnsureValid();
            RemoveAll(_pointer);
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is Container) return IsEqual(_pointer, (Object as Container)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Native Source Impl
        private class ContainerCollectionSource : NativeCollectionSource<Widget>
        {
            #region Variables
            private Container _container = null;
            #endregion

            #region Constructors
            public ContainerCollectionSource(Container Container)
            {
                _container = Container;
            }
            #endregion

            #region Functions
            public int GetItemCount()
            {
                _container.EnsureValid();
                return GetChildrenCount(_container._pointer);
            }
            public Widget GetItemByIndex(int Index)
            {
                _container.EnsureValid();
                return new Widget(GetChildByIndex(_container._pointer, Index));
            }
            #endregion
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgContainer_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyContainer(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgContainer_GetBaseWidget"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseWidget(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgContainer_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgContainer_Add"), SuppressUnmanagedCodeSecurity]
        private static extern void Add(IntPtr Pointer, IntPtr Widget);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgContainer_GetChildByIndex"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetChildByIndex(IntPtr Pointer, int Index);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgContainer_GetChildrenCount"), SuppressUnmanagedCodeSecurity]
        private static extern int GetChildrenCount(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgContainer_HandleChildInvalidate"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr HandleChildInvalidate(IntPtr Pointer, IntPtr Widget);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgContainer_IsChild"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsChild(IntPtr Pointer, IntPtr Widget);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgContainer_Remove"), SuppressUnmanagedCodeSecurity]
        private static extern void Remove(IntPtr Pointer, IntPtr Widget);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgContainer_RemoveAll"), SuppressUnmanagedCodeSecurity]
        private static extern void RemoveAll(IntPtr Pointer);
        #endregion
    }
}
