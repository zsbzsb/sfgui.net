﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class CheckButton : ToggleButton
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr CheckButtonPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        #endregion

        #region Constructors
        internal CheckButton() { }
        public CheckButton(string Label)
        {
            _pointer = CreateCheckButton(Label);
            base.Initialize(GetBaseToggleButton(_pointer));
        }
        protected override void Initialize(IntPtr Pointer)
        {
            _pointer = Pointer;
            base.Initialize(GetBaseToggleButton(Pointer));
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyCheckButton(_pointer);
            base.Destroy();
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is CheckButton) return IsEqual(_pointer, (Object as CheckButton)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgCheckButton_Create"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr CreateCheckButton(string Label);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgCheckButton_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyCheckButton(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgCheckButton_GetBaseToggleButton"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseToggleButton(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgCheckButton_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);
        #endregion
    }
}
