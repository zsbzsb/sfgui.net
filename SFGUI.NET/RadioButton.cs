﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public partial class RadioButton : CheckButton
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr RadioButtonPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        public RadioButtonGroup Group
        {
            get
            {
                EnsureValid();
                return new RadioButtonGroup(GetGroup(_pointer));
            }
            set
            {
                EnsureValid();
                SetGroup(_pointer, value.RadioButtonGroupPointer);
            }
        }
        #endregion

        #region Constructors
        public RadioButton(string Label, RadioButtonGroup Group = null)
        {
            _pointer = CreateRadioButton(Label, Group != null ? Group.RadioButtonGroupPointer : IntPtr.Zero);
            base.Initialize(GetBaseCheckButton(_pointer));
        }
        internal RadioButton(IntPtr Pointer)
        {
            _pointer = Pointer;
            base.Initialize(GetBaseCheckButton(_pointer));
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyRadioButton(_pointer);
            base.Destroy();
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is RadioButton) return IsEqual(_pointer, (Object as RadioButton)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButton_Create"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr CreateRadioButton(string Label, IntPtr GroupPointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButton_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyRadioButton(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButton_GetBaseCheckButton"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseCheckButton(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButton_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButton_GetGroup"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetGroup(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgRadioButton_SetGroup"), SuppressUnmanagedCodeSecurity]
        private static extern void SetGroup(IntPtr Pointer, IntPtr GroupPointer);
        #endregion
    }
}
