﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Window : Bin
    {
        #region Enums
        public enum Style : byte
        {
            NoStyle = 0,
            Titlebar = 1 << 0,
            Background = 1 << 1,
            Resize = 1 << 2,
            Shadow = 1 << 3,
            TopLevel = Titlebar | Background | Resize
        }
        #endregion

        #region Properties
        public FloatRect ClientRect
        {
            get
            {
                EnsureValid();
                return GetClientRect(_pointer);
            }
        }
        public Style WindowStyle
        {
            get
            {
                EnsureValid();
                return GetStyle(_pointer);
            }
            set
            {
                EnsureValid();
                SetStyle(_pointer, value);
            }
        }
        public string Title
        {
            get
            {
                EnsureValid();
                return Marshal.PtrToStringAnsi(GetTitle(_pointer));
            }
            set
            {
                EnsureValid();
                SetTitle(_pointer, value);
            }
        }
        #endregion

        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr WindowPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        #endregion

        #region Constructors
        public Window(Style Style = Style.TopLevel)
        {
            _pointer = CreateWindow(Style);
            base.Initialize(GetBaseBin(_pointer));
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyWindow(_pointer);
            base.Destroy();
        }
        public bool HasStyle(Style Style)
        {
            EnsureValid();
            return HasStyle(_pointer, Style);
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is Window) return IsEqual(_pointer, (Object as Window)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWindow_Create"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr CreateWindow(Style Style);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWindow_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyWindow(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWindow_GetBaseBin"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseBin(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWindow_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWindow_GetClientRect"), SuppressUnmanagedCodeSecurity]
        private static extern FloatRect GetClientRect(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWindow_GetStyle"), SuppressUnmanagedCodeSecurity]
        private static extern Style GetStyle(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWindow_GetTitle"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetTitle(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWindow_HasStyle"), SuppressUnmanagedCodeSecurity]
        private static extern bool HasStyle(IntPtr Pointer, Style Style);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWindow_SetStyle"), SuppressUnmanagedCodeSecurity]
        private static extern void SetStyle(IntPtr Pointer, Style Style);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgWindow_SetTitle"), SuppressUnmanagedCodeSecurity]
        private static extern void SetTitle(IntPtr Pointer, string Title);
        #endregion
    }
}
