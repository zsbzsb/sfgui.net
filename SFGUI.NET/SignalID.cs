﻿using System;

namespace SFGUINet
{
    public class SignalID
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        private SignalIDType _type;
        #endregion

        #region Properties
        internal IntPtr Pointer
        {
            get
            {
                return _pointer;
            }
            set
            {
                _pointer = value;
            }
        }
        internal SignalIDType Type
        {
            get
            {
                return _type;
            }
        }
        #endregion

        #region Constructors
        internal SignalID(SignalIDType Type)
        {
            _type = Type;
        }
        #endregion
    }
}
