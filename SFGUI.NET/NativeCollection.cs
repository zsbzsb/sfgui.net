﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace SFGUINet
{
    [DebuggerTypeProxy(typeof(IEnumerableDebugView<>)), DebuggerDisplay("Length = {Length}"), CustomDebuggerDisplay("Length = {Length}")]
    public class NativeCollection<T> : IEnumerable<T>
    {
        #region Variables
        private NativeCollectionSource<T> _source = null;
        #endregion

        #region Properties
        public int Length
        {
            get
            {
                return _source.GetItemCount();
            }
        }
        public T this[int Index]
        {
            get
            {
                return _source.GetItemByIndex(Index);
            }
        }
        #endregion

        #region Constructors
        internal NativeCollection(NativeCollectionSource<T> Source)
        {
            _source = Source;
        }
        #endregion

        #region Functions
        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < Length; i++)
            {
                yield return this[i];
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        #endregion
    }
}
