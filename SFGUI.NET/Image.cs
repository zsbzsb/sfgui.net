﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Image : Widget
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr ImagePointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        public SFML.Graphics.Image ActiveImage
        {
            get
            {
                EnsureValid();
                SFML.Graphics.Image img = new SFML.Graphics.Image(0, 0);
                GetImage(_pointer, img.CPointer);
                return img;
            }
            set
            {
                EnsureValid();
                SetImage(_pointer, value.CPointer);
            }
        }
        #endregion

        #region Constructors
        internal Image(IntPtr Pointer)
        {
            _pointer = Pointer;
            base.Initialize(GetBaseWidget(_pointer));
        }
        public Image(SFML.Graphics.Image Image)
        {
            _pointer = CreateImage(Image.CPointer);
            base.Initialize(GetBaseWidget(_pointer));
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyImage(_pointer);
            base.Destroy();
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is Image) return IsEqual(_pointer, (Object as Image)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgImage_Create"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr CreateImage(IntPtr ImagePointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgImage_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyImage(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgImage_GetBaseWidget"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseWidget(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgImage_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgImage_SetImage"), SuppressUnmanagedCodeSecurity]
        private static extern void SetImage(IntPtr Pointer, IntPtr ImagePointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgImage_GetImage"), SuppressUnmanagedCodeSecurity]
        private static extern void GetImage(IntPtr Pointer, IntPtr ImagePointer);
        #endregion
    }
}
