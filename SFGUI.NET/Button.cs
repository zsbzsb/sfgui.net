﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using SFML.Window;
using SFML.Graphics;

namespace SFGUINet
{
    public class Button : Bin
    {
        #region Variables
        private IntPtr _pointer = IntPtr.Zero;
        #endregion

        #region Properties
        internal IntPtr ButtonPointer
        {
            get
            {
                EnsureValid();
                return _pointer;
            }
        }
        public Image Image
        {
            get
            {
                EnsureValid();
                var imageptr = GetImage(_pointer);
                if (imageptr != IntPtr.Zero)
                {
                    return new Image(imageptr);
                }
                else return null;
            }
            set
            {
                EnsureValid();
                if (value == null) ClearImage(_pointer);
                else SetImage(_pointer, value.ImagePointer);
            }
        }
        public string Label
        {
            get
            {
                EnsureValid();
                return Marshal.PtrToStringAnsi(GetLabel(_pointer));
            }
            set
            {
                EnsureValid();
                SetLabel(_pointer, value);
            }
        }
        #endregion

        #region Constructors
        internal Button() { }
        public Button(string Label)
        {
            _pointer = CreateButton(Label);
            base.Initialize(GetBaseBin(_pointer));
        }
        protected override void Initialize(IntPtr Pointer)
        {
            _pointer = Pointer;
            base.Initialize(GetBaseBin(Pointer));
        }
        #endregion

        #region Functions
        protected override void Destroy()
        {
            DestroyButton(_pointer);
            base.Destroy();
        }
        protected override bool IsEqual(InternalBase Object)
        {
            if (Object is Button) return IsEqual(_pointer, (Object as Button)._pointer);
            else return base.IsEqual(Object);
        }
        #endregion

        #region Imports
        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgButton_Create"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr CreateButton(string Label);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgButton_Destroy"), SuppressUnmanagedCodeSecurity]
        private static extern void DestroyButton(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgButton_GetBaseBin"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetBaseBin(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgButton_ClearImage"), SuppressUnmanagedCodeSecurity]
        private static extern void ClearImage(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgButton_GetImage"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetImage(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgButton_SetImage"), SuppressUnmanagedCodeSecurity]
        private static extern void SetImage(IntPtr Pointer, IntPtr ImagePointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgButton_GetLabel"), SuppressUnmanagedCodeSecurity]
        private static extern IntPtr GetLabel(IntPtr Pointer);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgButton_SetLabel"), SuppressUnmanagedCodeSecurity]
        private static extern void SetLabel(IntPtr Pointer, string Label);

        [DllImport(Globals.CSFGUI_DLL, CallingConvention = CallingConvention.Cdecl, EntryPoint = "sfgButton_IsEqual"), SuppressUnmanagedCodeSecurity]
        private static extern bool IsEqual(IntPtr A, IntPtr B);
        #endregion
    }
}
